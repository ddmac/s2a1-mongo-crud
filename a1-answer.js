// Create 5 channels Blue Team, Red, Yellow, Black and White with an initial 3 members each
// Note: Messages, isActive and likes will be added later
// use chatAppDB;
db.channels.insertMany([
	{
	    channelName: "Blue Team",
	    members: ["Harry", "Ron", "Hermione"],
	},
	{
	    channelName: "Red Team",
	    members: ["Harry", "Ron", "Hermione"],
	},
	{
	    channelName: "Yellow Team",
	    members: ["Harry", "Ron", "Hermione"],
	},
	{
	    channelName: "Black Team",
	    members: ["Harry", "Ron", "Hermione"],
	},
	{
	    channelName: "White Team",
	    members: ["Harry", "Ron", "Hermione"],
	}
]);

// Add 5 new messages to each channel
db.channels.update(
	{
	    channelName: "Blue Team"
	},
	{
		$set: 
		{
			messages: 
		    [
		        {
		            _id: 1,
		            sender: "Harry",
		            message: "Welcome to the Blue Team!",
		            postedDate: "05/13/2020",
		        },
		        {
		            _id: 2,
		            sender: "Ron",
		            message: "Hi",
		            postedDate: "05/13/2020",
		        },
		        {
		            _id: 3,
		            sender: "Hermione",
		            message: "Hello",
		            postedDate: "05/13/2020",
		        },
		        {
		            _id: 4,
		            sender: "Harry",
		            message: "How are you all?",
		            postedDate: "05/13/2020",
		        },
		        {
		            _id: 5,
		            sender: "Hermione",
		            message: "Good, thank you!",
		            postedDate: "05/13/2020",
		        }
		    ]
		}
	}
);

db.channels.update(
	{
	    channelName: "Red Team"
	},
	{
		$set: 
		{
		    messages: 
		    [
		        {
		            _id: 6,
		            sender: "Tito",
		            message: "Welcome to the Red Team!",
		            postedDate: "05/13/2020",
		        },
		        {
		            _id: 7,
		            sender: "Vic",
		            message: "Salamat!",
		            postedDate: "05/13/2020",
		        },
		        {
		            _id: 8,
		            sender: "Joey",
		            message: "Go Red team!",
		            postedDate: "05/13/2020",
		        },
		        {
		            _id: 9,
		            sender: "Tito",
		            message: "Kamusta?",
		            postedDate: "05/13/2020",
		        },
		        {
		            _id: 10,
		            sender: "Vic",
		            message: "Mabuti!",
		            postedDate: "05/13/2020",
		        }
		    ]
		}
	}
);


db.channels.update(
	{
	    channelName: "Yellow Team"
	},
	{
		$set: 
		{
		    messages: 
		    [
		        {
		            _id: 11,
		            sender: "Aida",
		            message: "Welcome to the Yello Team!",
		            postedDate: "05/13/2020",
		        },
		        {
		            _id: 12,
		            sender: "Lorna",
		            message: "Hi",
		            postedDate: "05/13/2020",
		        },
		        {
		            _id: 13,
		            sender: "Fe",
		            message: "Hello",
		            postedDate: "05/13/2020",
		        },
		        {
		            _id: 14,
		            sender: "Aida",
		            message: "How are you all?",
		            postedDate: "05/13/2020",
		        },
		        {
		            _id: 15,
		            sender: "Fe",
		            message: "Good, thank you!",
		            postedDate: "05/13/2020",
		        }
		    ]
		}
	}
);


db.channels.update(
	{
	    channelName: "Black Team"
	},
	{
		$set: 
		{
		    messages: 
		    [
		        {
		        	_id: 16,
		            sender: "Leonardo",
		            message: "Welcome to the Black Team!",
		            postedDate: "05/13/2020",
		        },
		        {
		        	_id: 17,
		            sender: "Rafael",
		            message: "Why black?",
		            postedDate: "05/13/2020",
		        },
		        {
		        	_id: 18,
		            sender: "Donatello",
		            message: "Yeah, why black?",
		            postedDate: "05/13/2020",
		        },
		        {
		        	_id: 19,
		            sender: "Leonardo",
		            message: "We should be green, right?",
		            postedDate: "05/13/2020",
		        },
		        {
		        	_id: 20,
		            sender: "Donatello",
		            message: "Yes!",
		            postedDate: "05/13/2020",
		        }
		    ]
		}
	}
);


db.channels.update(
	{
	    channelName: "White Team"
	},
	{
		$set: 
		{
		    messages: 
		    [
		        {
		        	_id: 21,
		            sender: "MJ",
		            message: "Welcome to the White Team!",
		            postedDate: "05/13/2020",
		        },
		        {
		        	_id: 22,
		            sender: "Lebron",
		            message: "Yo",
		            postedDate: "05/13/2020",
		        },
		        {
		        	_id: 23,
		            sender: "Kobe",
		            message: "Sup",
		            postedDate: "05/13/2020",
		        },
		        {
		        	_id: 24,
		            sender: "MJ",
		            message: "Let's play!",
		            postedDate: "05/13/2020",
		        },
		        {
		        	_id: 25,
		            sender: "Kobe",
		            message: "Game!",
		            postedDate: "05/13/2020",
		        }
		    ]
		}
	}
);


// Add an isActive property to all channels and set it to true
db.channels.updateMany(
	{
	
	},
	{
		$set: 
		{
		    isActive: true
		}
	}
);

//Change Yellow and Black team to inactive
db.channels.update(
	{
	    channelName: "Yellow Team"
	},
	{
		$set: 
		{
		    isActive: false
		}
	}
);

db.channels.update(
	{
	    channelName: "Black Team"
	},
	{
		$set: 
		{
		    isActive: false
		}
	}
);

//Remove the channel white team
db.channels.remove(
	{
	    channelName: "White Team"
	}
);

//Display all active channels
db.channels.find(
	{
		isActive: true
	}
);

//Create a query that removes all inactive channels
db.channels.remove(
	{
	    isActive: false
	}
);

//Create a query that removes a specific message in a channel
db.channels.update({'_id': ObjectId("5ebbb562e750de0830f8e51f")}, {$unset: {"messages.0":""}});

//Add a property which will contain a list of member names who liked a message
db.channels.update(
	{
	    channelName: "Blue Team"
	},
	{
		$set: 
		{
		    "messages.0.likes": []
		}
	}
);

//Create a query which will add a name to the list of member names who liked a message
db.channels.update(
	{
	    channelName: "Blue Team"
	},
	{
		$set: 
		{
		    "messages.0.likes": ["Hermione", "Ron"]
		}
	}
);